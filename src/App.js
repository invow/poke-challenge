/**
 * @Dependencies
 */
import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { styles } from "./styles";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import axios from "axios";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CardHeader from "@material-ui/core/CardHeader";
import CardActions from "@material-ui/core/CardActions";

/**
 * App is an abstract base class. It is defined simply
 * to have a users searcher
 * @constructor
 */
class App extends React.Component {
  /**
   * @constructor
   * @Params props: React props
   */
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      expanded: false,
      shown: {},
      pokemones: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleExpandClick = this.handleExpandClick.bind(this);

    /**
     * @Call Random User API
     */
    axios.get("https://pokeapi.co/api/v2/pokemon").then((response) => {
      this.setState({ pokemones: response.data.results });
    });
  }

  /**
   * @handleExpandClick
   * Expand / Collapse items
   */
  handleExpandClick(index) {
    this.setState({
      shown: {
        [index]: !this.state.shown[index],
      },
    });
  }

  /**
   * @handleChange
   * Set state for name search results
   * @Params event: $event
   */
  handleChange(event) {
    this.setState({ name: event.target.value });
  }

  /**
   * @render
   * React render
   */
  render() {
    /**
     * @Match results
     * Displays matched results
     */
    let libraries = this.state.pokemones;
    let searchString = this.state.name.trim().toLowerCase();
    if (searchString.length > 0) {
      libraries = libraries.filter(function (i) {
        return i.name.toLowerCase().match(searchString);
      });
    }

    /**
     * @return
     * Renders return
     */
    return (
      <div className="App">
        <header className="App-header">
          <AppBar position="static">
            <Toolbar>
              <img src={logo} className="App-logo" alt="logo" />
            </Toolbar>
          </AppBar>
        </header>
        <form noValidate autoComplete="off">
          <TextField
            className="searcher"
            id="standard-name"
            label="Search users"
            placeholder="First name"
            onChange={this.handleChange}
            margin="normal"
          />
        </form>
        {libraries &&
          libraries.map((pokemones, index) => (
            <>
              <Card className="fix-card">
                <CardHeader
                  title={pokemones.name}
                />
                <a href={pokemones.url} target="_blank" rel="noopener noreferrer">
                  {pokemones.url}
                </a>
                <CardActions disableActionSpacing />
              </Card>
              <div></div>
            </>
          ))}
      </div>
    );
  }
}
export default withStyles(styles)(App);
